\documentclass[letterpaper, 11 pt, conference]{article}  % Comment this line out
                                                          % if you need a4paper
%\documentclass[a4paper, 10pt, conference]{ieeeconf}      % Use this line for a4
                                                          % paper

% \IEEEoverridecommandlockouts                              % This command is only
                                                          % needed if you want to
                                                          % use the \thanks command
% \overrideIEEEmargins
% See the \addtolength command later in the file to balance the column lengths
% on the last page of the document

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{listings}
\usepackage[utf8]{inputenc}
 
\usepackage{listings}
\usepackage{xcolor}
\usepackage{graphicx}
 
\definecolor{codegreen}{rgb}{0,0.6,0}
\definecolor{codegray}{rgb}{0.5,0.5,0.5}
\definecolor{codepurple}{rgb}{0.58,0,0.82}
\definecolor{backcolour}{rgb}{0.95,0.95,0.92}
 
\lstdefinestyle{mystyle}{
    backgroundcolor=\color{backcolour},   
    commentstyle=\color{codegreen},
    keywordstyle=\color{magenta},
    numberstyle=\tiny\color{codegray},
    stringstyle=\color{codepurple},
    basicstyle=\ttfamily\footnotesize,
    breakatwhitespace=false,         
    breaklines=true,                 
    captionpos=b,                    
    keepspaces=true,                 
    numbers=left,                    
    numbersep=5pt,                  
    showspaces=false,                
    showstringspaces=false,
    showtabs=false,                  
    tabsize=2
}
 
\lstset{style=mystyle}

\def\code#1{\texttt{#1}}

% The following packages can be found on http:\\www.ctan.org
%\usepackage{graphics} % for pdf, bitmapped graphics files
%\usepackage{epsfig} % for postscript graphics files
%\usepackage{mathptmx} % assumes new font selection scheme installed
%\usepackage{mathptmx} % assumes new font selection scheme installed
%\usepackage{amsmath} % assumes amsmath package installed
%\usepackage{amssymb}  % assumes amsmath package installed

\title{\LARGE \bf
Conway's Game of Life in Parallel
}

\author{  Bosco Ndemeye, Shant Hairapetian }




\begin{document}



\maketitle
\thispagestyle{empty}
\pagestyle{empty}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{abstract}
Though many heralding the end of Moore's law have been accused of crying wolf,
the consensus seems to be that it is in fact in its twilight years\cite{c1}. Our processors have gotten about as fast as they're going to get while the demand
for high-performance software is ever growing. Our only way forward currently 
is to increase the number of processors and split the work amongst them. The GPU (Graphics Processing Unit) is the manifestation of this idea and Nvidia's CUDA is the most popular library for programming them. CUDA extends C, C++ and FORTRAN to include parallelism primitives. These languages are considered low-level system languages because of the control they give a developers over the hardware. This is double-edged sword because although this fine grained  control empowers developers to do aggressive performance optimization, it makes it easy to introduce bugs into the code\cite{c2}. Our goal was to find a technology which allowed us to express parallel programs at a high level
and let the compiler take care of the nitty-gritty details of performance
optimization. The technology we picked Accelerate\cite{c3}, a Domain Specific Language built on top the Haskell programming language which compiled to a CUDA C backend. Our aim was to show that writing parallel code in such a high-level and expressive language would make the code easier to reason about and thus less prone to errors. Though this was our theory it turned out not be true. It was even more difficult to implement our parallel program in Accelerate and the code was even less readable. We found once we implemented the CUDA version of our algorithm that our problem was not the readability of the code but the constraints on our problem size: any data set larger than 1 billion
records would cause a segmentation fault. Thus we sought a technology
that would allow us to distribute our parallel program across 
multiple nodes.
\end{abstract}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{INTRODUCTION}
\subsection{Conway's Game of Life}
Conway’s Game of Life is an algorithm which given a rectangular board of cells 
which are marked living or dead, calculates whether a cell lives on to the next 
iteration. The decision about whether a cell lives or dies is informed by the 
following rules:

\begin{itemize}

\item A cell with less than two neighbors dies 
\item A cell with more than three neighbors dies
\item A living cell with two or three neighbors lives on
\item A dead cell with three living neighbors is made brought back to life
\end{itemize}

The algorithm must loop through every cell in the board and based on the above 
rules make a decision as to which cells live on and which do not. In the original 
specification the board is infinite however due to practical considerations we will 
be implementing a wrapping board (meaning that any access that goes beyond the board 
 will wrap to the opposite side of the board). Because the checks for whether a cell lives 
or dies only concerns the cells immediately surrounding it, the board can be broken 
into chunks and the algorithm parallelised.

\subsection{CUDA}
CUDA is a GPGPU (General Purpose GPU Programming) library which empowers C and C++
developers with parallelism primitives facilitating the creation of heterogeneous 
programs which can run both on the CPU as well on the GPU in parallel. As the GPU
(device) and CPU (host) have distinct memory spaces, any data which needs to be 
processed by the devices must first be copied to the device and then the resulting data
must then be copied back. This overhead\cite{c4} in addition to that of spinning up the 
large number of threads involved in massively parallel programs makes it such that in 
order to see performance gain through parallelization, one must ensure that the data 
sets being analyzed are sufficiently large to reach the inflection of performance.




\section{Method}
\subsection{Serial Implementation}
In order to establish a baseline for benchmarking, we first implemented a naive serial 
algorithm. This algorithm encodes each cell as an unsigned byte of value 0 (dead) or 
1 (living) then iterates through all of the cells in the board using doubly nested for loops 
which iterate through the y and x coordinates respectively. In the inner loop we calculate 
for every cell the positions of its neighbors using modular arithmetic to account for 
neighbor wrapping (jumping to the opposite side of the board for border cases) and then 
sum the total of living neighbors. This is seen below:

\begin{lstlisting}[language=C]
int liveNeighbors = 
    b[x + y_top] + b[x + y_bot] 
    + b[x_lft + y] + b[x_lft + y_top] 
    + b[x_lft + y_bot] + b[x_rgt + y] 
    + b[x_rit + y_top] + b[x_rgt + y_bot];
\end{lstlisting}
The rules for whether a cell survives is then encoded in an inline conditional statement
seen below:
\begin{lstlisting}[language=C]    
(liveNeighbors == 2 || 
(liveNeighbors == 3  && input[x + y] == 1)) 
    ? 1:0;
\end{lstlisting}

\subsection{CUDA First Pass}
The first step in parallelizing the serial implementation is to flatten the board and
represent using a single dimension array. This is done by using modular arithmetic and 
multiplication to index to the correct position in the flat array board. The formula for 
converting coordinates between the two encodings is seen below:

\begin{lstlisting}    
    b[x][y] = b[x + y * (board_width)]
\end{lstlisting}

This flat array encoding allows us to break up the board into chunks which can then be processed
in parallel. We then construct a for loop which calculates the initial index by multiplying the
block size of the kernel with the blockId (index) (thereby calculating the position of the first
cell in a block) and adding the threadId (effectively splitting up the work of processing cells
within a block between all threads in that block). The for loop then increments the index by the
the total size of the grid. This means that the first thread executed will process the first 
element of every grid, the second will process the second element and so on. The reasons for this
are multifold: first and foremost, processing data by grids allows you have to datasets larger
than the maximum size of a grid as dictated by your CUDA device. This allows you to process data-
sets of any size. The second reason is that facilitates an optimization known as global memory
coalescing\cite{c5}. All this means is that the runtime groups together accesses of contiguous areas in
memory thereby reducing the latency associated with memory access. By incrementing the index 
by the total number of threads available in a grid, we guarantee that each subsequent invocation
of the kernel accesses the area in memory right next to the memory accessed by the previous 
kernel. This results in contiguous memory access and thus global memory coalescing.

\subsection{CUDA Bitwise Optimization}
The next step we can take to squeeze even more performance from our GPU, is to change the encoding
of the board to use individual bits for cells rather than an entire byte\cite{c6}. This will cut down 
the latency in board access (theoretically by a factor of 8) as we will be evaluating 8 cells at
a time. In order to accomplish this we load a slice of the board into three 32-bit unsigned integers.
One for the row of the cell we are examining and 2 additional integers for the rows above and below. 
As we need access to the byte before and after the byte we are currently examining we must load 9 
bytes of the board into memory. 1 byte for the row of cells we are evaluating, 2 bytes for the rows 
above and below that row and 3 more rows (bytes) on either side such that we can view the neighbors 
of the cells on the edges of the slice of the board that we are evaluating. We are forced to load 
the entire surrounding bytes as we can only load 8 bits at a time. To mitigate the overhead of 
loading 6 extra bytes into memory we can evaluate more than a single byte at a time while still 
only loading the surrounding 6 bytes into memory. In order to load bytes of the board into the
our integer rows we can simply cast the first byte to a uint32 then load subsequent bytes by OR’ing
the existing integer with the new byte left shifted by 8 (thereby pushing the first byte back to
make room). Once we have our rows loaded we can apply a bitmask over each of the neighbors and
left shift each bit until it is the binary value 1 or 0. The number of left shifts for this is 
dependent on the position of the bit (14, 15 or 16). These bites are then summed up to 
obtain the total number of live neighbors. The code for this is shown below:

\begin{lstlisting}[language=C]    
uint liveNeighbors = 
  (top_row & 0x200000 >> 16) + (top_row & 0x100000 >> 15)+ 
  (top_row & 0x40000 >> 14) + (middle_row & 0x200000 >> 16)+ 
  (bottom_row & 0x40000 >> 14) + (bottom_row & 0x200000 >> 16)+ 
  (bottom_row & 0x100000 >> 15) + (bottom_row & 0x40000 >> 14);
\end{lstlisting}


\subsection{OpenMP + MPI}
Starting with our serial implementation, we can use openMP's model for parallelism to parallelize our code on a shared memory architecture. However, we wanted to go a step further and explore what problems arise when you try to parallelize on a distributed memory system.
\subsubsection{OpenMP}
The heart of our computation can be written down as follows:
\begin{lstlisting}
for(int i = 0; i < iterations; i++){
    for(int y = 0; y < height; y++){
        for(int x = 0; x < width; x++){
            output[XCENTER + YCENTER] = isCellAlive(input, 
                                                    XCENTER, 
                                                    XLEFT, 
                                                    XRIGHT,
                                                    YCENTER, 
                                                    YTOP, 
                                                    YBOTTOM);  
        }
    }
    swap(input, output);
}
\end{lstlisting}
Where \textit{isCellAlive} is defined as described above and \textit{XCENTER, YCENTER, XLEFT, XRIGHT, YCENTER, YTOP, YBOTTOM} are macros for the neighborhood. The swap at the end of every iteration imposes serial execution for the iterations thus forbidding us to parallelize over iterations. Consequently, we can proceed to parallelize the inner for loops. We have three choices. We can either parallelize over rows, over columns, or both. The obvious choice would be to do both, but we run the risk of scheduling too many threads and the overhead that comes with that. Moreover, due to how our problem array is stored in memory, we choose to parallelize over rows and get:
\begin{lstlisting}
for(int i = 0; i < iterations; i++){
    #pragma omp parallel for schedule(static)
    for(int y = 0; y < height; y++){
        for(int x = 0; x < width; x++){
            output[XCENTER + YCENTER] = isCellAlive(input, 
                                                    XCENTER, 
                                                    XLEFT, 
                                                    XRIGHT,
                                                    YCENTER, 
                                                    YTOP, 
                                                    YBOTTOM);  
        }
    }
    swap(input, output);
}
\end{lstlisting}
We use the \textit{static} scheduler, because we want every thread to have a balanced workload.

The \textit{swap} at the end of every iteration in the code snippet above enforces a sequential ordering prohibiting the possibility of parallelizing over iterations. Amdahl's law informs us that, for a given fixed size problem no matter how much computing resources we throw at it, the amount of parallelism we can hope to gain from our problem is limited by its sequential part. In our specific case, the number of iterations. Gustafson's law assures us however, that increasing the problem size for large machines can retain scalability with respect to the number of processors. The problem then becomes how to modify our algorithm to take advantage of as much computing power as we can.
As a consequence of this, we explore how we can use the \textit{Message Passing Interface (MPI)} to exploit our resources as we scale the problem and still retain a reasonable runtime. 
\subsubsection{MPI}
Whereas parallelizing our algorithm with openMP withing a shared memory framework was straightforward, extra care needs to be taken on a distributed memory machine. Our problem dictates that every cell needs access to all of its surrounding cell neighbors to update its value for the next iteration. With that in mind, consider figure (\ref{fig:my_label}).
\begin{figure}[h]
    \centering
    \includegraphics[scale=0.3]{ghosts.jpg}
    \caption{Cells at edge of partition need ghost cells for updates}
    \label{fig:my_label}
\end{figure}

As the figure illustrates, regardless of how we partition our data, cells at the edge of a partition need access to neighboring cells assigned to a different partition. These cells, termed ``ghost" cells, need to be updated on every iteration. This implies communication between MPI threads. As this involves sending messages across a network, it can become a bottleneck quick. Consequently, we chose to limit the amount of communication we have to do per iteration and divide up our problem grid into sets of rows. That is, every MPI thread is assigned a set of entire rows. This means that only the top, and bottom row in that set needs to be communicated per iteration. Consequently, the same loop that forms the core of our computation becomes as shown in the code snippet below:


\begin{lstlisting}
MPI_Scatter(global_cells, local_size, MPI_UNSIGNED_CHAR, 
           local_cells, local_size, MPI_UNSIGNED_CHAR, 
           0, MPI_COMM_WORLD);
for(int it = 0; it < iterations; it++){
    if(world_rank != world_size - 1){
        MPI_Send(to_send_top, local_width, 
                 MPI_UNSIGNED_CHAR, world_rank + 1, 
                 1, MPI_COMM_WORLD);
    }
    if(world_rank != 0){
        MPI_Recv(lg_bottom, local_width, 
                 MPI_UNSIGNED_CHAR, world_rank - 1, 
                 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        MPI_Send(to_send_bottom, local_width, 
                 MPI_UNSIGNED_CHAR, world_rank - 1, 
                 0, MPI_COMM_WORLD);
    }

    if(world_rank != world_size - 1){
        MPI_Recv(lg_top, local_width, 
                 MPI_UNSIGNED_CHAR, world_rank + 1, 
                 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    }
    #pragma omp parallel for schedule(static) num_threads(10)
    for(int y = 0; y < local_height; y++){ 
        for(int x = 0; x < local_width; x++){
            local_output[XCENTER + YCENTER] 
                    = isCellAlive(local_cells, 
                                  lg_top, 
                                  lg_bottom,
                                  local_height,
                                  XCENTER, XLEFT, XRIGHT,
                                  YCENTER, YTOP, YBOTTOM);  
            }
    }  
    
    
    update_to_send(to_send_top, 
                       to_send_bottom, 
                       local_cells, 
                       local_height, 
                       local_width);
    MPI_Barrier(MPI_COMM_WORLD); 
    if(it != iterations - 1){
        swap(local_cells, local_output);
    }
}
MPI_Gather(local_output, local_size, 
           MPI_UNSIGNED_CHAR, global_output, 
           local_size, MPI_UNSIGNED_CHAR, 
           0, MPI_COMM_WORLD);
\end{lstlisting}
Where \textit{local\_cells} and \textit{local\_output} are the local input and output buffers respectively, for each MPI rank. \textit{to\_send\_top} and \textit{to\_send\_bottom} are buffers that hold ghost cells when they are ready to be sent. Buffers \textit{lg\_top} and \textit{lg\_bottom} hold ghost cells. \textit{local\_width} is just another name for width, but \textit{local\_height} is \textit{height} diveded by \textit{world\_size}. 

We need to synchronize MPI threads at the end of every iteration to make sure every thread has populated ghost cell buffers it is going to send before sending it, hence the MPI\_Barrier.

In addition to using the \textit{static} openMP scheduler, we request the number of threads used to be 10. This can number can be changed, but as for our specific case we request to use 10 CPUs for a MPI task when we submit our job, this makes sure the software and hardware threads are well mapped.
\section{Results}
\subsection{CUDA Results}
\subsubsection{Correctness}
\begin{itemize}
    \item \textbf{Memory Coalesced Kernel}
    We were able to verify the correctness of this kernel by comparing the output
    for various values of n to the output of the serial algorithm. Though this is not a foolproof verification method (as it is possible that the serial implementation is also incorrect) the GPU implementation differs enough from the serial that the chances of them both being incorrect in the same way is statistically unlikely.
    \item \textbf{Bitwise Kernel}
    Though we were able to test its correctness with small, manually created boards, due to time constraints we were unable to successfully complete the bytewise to bitwise board converter. As such we were not able to verify that the bitwise kernel converges with the serial algorithm. 
\end{itemize}
\subsubsection{Performance}
    \begin{table}[h]
    \centering
    \begin{tabular}{|c|c|c|}
    \hline
                & Memory Coalesced & Bit-wise Optimization \\ \hline
    3,840       & -1035x           & 1x                    \\ \hline
    38,400      & -115x            & 8x                    \\ \hline
    384,000     & -12x             & 10x                   \\ \hline
    3,840,000   & -1x              & 24x                   \\ \hline
    38,400,000  & 7x               & 30x                   \\ \hline
    384,000,000 & 26x              & 33x                   \\ \hline
    \end{tabular}
    \end{table}
    As the parallelization of this algorithm is only relevant for a single iteration, speed does not increase as the number of iterations do. As such we conducted our testing with a single iteration. As we can see the memory coalesced kernel does converge with speedup until somewhere between 3 and 30 million records while the bitwise optimization overcomes the memory latency and thread creation overhead quickly at only 3,840 records. We see speedup increase as n grows until a maximum speedup of 28x from the memory coalesced kernel and the 33x from the bitwise implementation. There is an inexplicably large (at least to us) jump in performance in the memory coalesced version between 38,400,000 and 384,000,000 records. Any data set larger than a billion or so would cause TALAPAS to segfault.
\subsection{OpenMP + MPI Results}
\subsubsection{Correctness}
\begin{itemize}
    \item \textbf{OpenMP}
    A property of the Game of Life automaton is that its transition function is a pure function. That is, given the same input, it will always produce the same output. Consequently, we calculate an initial checksum for our array, before the serial run, and calculate one after each iteration  of the serial run. The sum of these checksums gives us a base value.
    
    Consequently, for our openMP implementation, we can similarly calculate a checksum before the the computation, and a checksum after every iteration. We can then sum all those values up and compare the result to our base value.
    
    \item \textbf{MPI}
    We proceed with the same method to test for correctness of our MPI implementation. However, every MPI rank keeps its own checksum, and so to calculate the global checksum we use MPI\_REDUCE with the 0 rank as root thereby getting the total sum after all iterations are done on all partitions.
\end{itemize}
\subsubsection{Performance}
\begin{itemize}
    \item \textbf{OpenMP}
    Using 14 threads and the static scheduler, we were able to get 10.3x average speedup on talapas. This is for a problem size of 5000 iterations, width of 1000 and height of 1000.
    On a single node this is close to optimal as we have 14 cores that can share a socket at the same time. Therefore, we think it is reasonable that the other runtime was taken up by the not so small portion of our problem.
    
    \item \textbf{MPI}
    For the same problem size, using all of 56 CPUs on talapas, our MPI implementation runs, on average, for 21 (s). This is almost twice as slow as the (non-distributed) openMP implementation. But this is to be expected as there is overhead associated with communicating ghost cells back and forth. However this is still almost a factor of 5x faster than the serial implementation.
    
    Consequently, one would be tempted to ask why the need for the distributed version of the automata in the first place if it is always going to be slower than the openMP version that's not distributed. However, increasing our problem size from 1000 x 1000 and 5000 iterations to to 100000 x 1000 and 100 iterations reveals why. A problem of this size will not run within the shared memory framework that talapas supports. However, our implementation of the distributed version still runs on there and does so in less than 100 (s), beating the serial implementation for the 1000 x 1000 problem. Moreover, even the GPU implementation that is not distributed will run into a segfault on this problem. Therefore, we are assured that our distributed implementation was not a total waste of time.
    
    
\end{itemize}


\section{Conclusion}
It is apparent to us that there is still much PL work to be done in the field. Accelerate makes heavy use of strong dependent types that in our opinion makes it harder to learn for new comers. Moreover, its lack of support of nested data parallelism forces the programmer to not write the natural functional algorithm thereby sacrificing readability and going against Accelerate's motivation. Maybe it was the limited amount of time we had to pick it up and implement a solution, but our first impression is that it does not make good on its promise. We have an implementation that passes the typechecker and so one would think is error free. That is not the case however and we run into an out of bounds exception that to our best ability we were not able to figure out why and where it was it was comming from. It was much simpler for us to implement and test our CUDA implementations
than it was to write Accelerate code. Given that both of us specialize in PL and type driven development, our difficulty was especially damning. 
It turns that our most pressing issue was not with readability but with scaling. Though we were able to see modest speedup up to half a billion or records or so, data sets larger than
a billion caused segmentation faults. However, with the use of MPI we were able to get around the issue even if that required careful thought on its own. Overall, we were able to explore what issues have to be considered as a seemingly simple problem like the game of life scales, and our explorations bore fruit.


\addtolength{\textheight}{-12cm}   % This command serves to balance the column lengths
                                  % on the last page of the document manually. It shortens
                                  % the textheight of the last page by a suitable amount.
                                  % This command does not take effect until the next page
                                  % so it should come on the page before the last. Make
                                  % sure that you do not shorten the textheight too much.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{thebibliography}{99}


\bibitem{c1} T. N. Theis and H. -. P. Wong, "The End of Moore's Law: A New Beginning for Information Technology," in Computing in Science \& Engineering, vol. 19, no. 2, pp. 41-50, Mar.-Apr. 2017.
\bibitem{c2}Ray, Baishakhi \& Posnett, Daryl & Filkov, Vladimir \& Devanbu, Premkumar. (2014). A large scale study of programming languages and code quality in github. Proc. FSE 2014. 155-165. 10.1145/2635868.2635922.
\bibitem{c3} Chakravarty, Manuel \& Keller, Gabriele \& Lee, Sean \& McDonell, Trevor \& Grover, Vinod. (2011). Accelerating Haskell array codes with multicore GPUs. DAMP'11 - Proceedings of the 6th ACM Workshop on Declarative Aspects of Multicore Programming. 3-14. 10.1145/1926354.1926358.
\bibitem{c4} Harris, Mark, and Mark. “How to Optimize Data Transfers in CUDA C/C++.” NVIDIA Developer Blog, 2 May 2018, devblogs.nvidia.com/how-optimize-data-transfers-cuda-cc/.
\bibitem{c5} Kim, Dae-Hwan. “Evaluation Of The Performance Of GPU Global Memory Coalescing.” (2017). 
\bibitem{c6} Fišer, Marek. “Conway's Game of Life on GPU Using CUDA.” Conway's Game of Life on GPU Using CUDA: Basic Implementation – MarekFiser.com, 2013, www.marekfiser.com/Projects/Conways-Game-of-Life-on-GPU-using-CUDA/3-Advanced-bit-per-cell-implementation




\end{thebibliography}



\end{document}