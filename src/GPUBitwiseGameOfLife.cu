#include <stdlib.h>
#include <cstdint>
#include <iterator>
#include <algorithm>
#include "common.c"

typedef unsigned char ubyte;

__global__ void gameOfLifeKernelB(const ubyte* input, uint width, uint height, uint bytesInThread, ubyte* output)
{
    uint size = width * height;
    for(
        uint eId = (__mul24(blockIdx.x, blockDim.x) + threadIdx.x) * bytesInThread; 
        eId < size;
        eId += __mul24(blockDim.x, gridDim.x) * bytesInThread
    ){
        // start at index -1, so that we can use the data to the left
        // to calculate the values of the border values
        uint x_center = (eId + width - 1) % width;
        uint y_center = (eId / width) % width;
        uint y_above  = (y_center + size - width) % size;
        uint y_below  = (y_center + width) % size;

        // allocate 1 byte per row and make room for 1 more
        uint top_row    = (uint)input[x_center + y_above] << 16;
        uint middle_row = (uint)input[x_center + y_center] << 16;
        uint bottom_row = (uint)input[x_center + y_below] << 16;

        x_center = (x_center + 1) % width;

        // allocating second byte
        top_row    = top_row    | (uint)input[x_center + y_above] << 8;
        middle_row = middle_row | (uint)input[x_center + y_center] << 8;
        bottom_row = bottom_row | (uint)input[x_center + y_below] << 8;

        for(uint i = 0; i < bytesInThread; i++){
            int last_x = x_center;
            x_center = (x_center + 1) % width;

            top_row    = top_row    | (uint)input[x_center + y_above];
            middle_row = middle_row | (uint)input[x_center + y_center];
            bottom_row = bottom_row | (uint)input[x_center + y_below];

            

            uint result_bytes = 0;
            for(uint j = 0; j < 8; j++){
                
                uint liveNeighbors = 
                    (top_row & 0x200000 >> 16) + (top_row & 0x100000 >> 15) + (top_row & 0x40000 >> 14) +
                    (middle_row & 0x200000 >> 16) + (bottom_row & 0x40000 >> 14) +
                    (bottom_row & 0x200000 >> 16) + (bottom_row & 0x100000 >> 15) + (bottom_row & 0x40000 >> 14);
            
                liveNeighbors = liveNeighbors >> 14;

                result_bytes = result_bytes << 1 | (liveNeighbors == 2  && (middle_row & 0x8000u) || liveNeighbors == 3) ? 1u : 0u;

                top_row    = top_row << 1;
                middle_row = middle_row << 1;
                bottom_row = bottom_row << 1;

            }

            output[last_x + y_center] = result_bytes;

        }


    }

}

void runKernel(ubyte*& input, ubyte*& output, size_t width, size_t height, size_t iterationsCount, ushort threadsCount) {
    size_t blocksCount = (width * height) / threadsCount;
    
    for (size_t i = 0; i < iterationsCount; ++i) {
        gameOfLifeKernelB<<<blocksCount, 256>>>(input, width, height, 16, output);
        std::swap(input, output);
    }
}
