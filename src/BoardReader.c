#include <stdlib.h> 
#include <stdio.h>
#include <time.h>

typedef unsigned char ubyte;

struct board
{
    int size;
    int width;
    ubyte* cells;
};

// srand(time(NULL));   

void gen_random_board(struct board* b)
{
    srand(time(NULL)); 
    for(int i = 0; i < b -> size; i++)
    {
        ubyte r  = rand() % 2;
        b -> cells[i] = r;
    }
    
}

ubyte *strToUByteArray(char *str, int len)
{
    int i = 0;
    ubyte *ret = (ubyte *)malloc(len * sizeof(ubyte));
    while (str[i] != '\0')
    {
        ret[i] = (str[i] == '0') ? 0 : 1;
        i++;
    }
    return ret;
}

char *getNextDollarDelimtedValue(FILE *f, int len)
{
    int c, i = 0;
    char *value = (char *)malloc(len * sizeof(char));

    while ((c = getc(f)) && c != EOF && c != '$')
    {
        value[i++] = c;
    }
    value[i] = '\0';
    return value;
}

struct board readBoard(char* filePath)
{
    FILE *f;
    int size;
    int width;
    f = fopen(filePath, "r");
    size = atoi(getNextDollarDelimtedValue(f, 10));
    width = atoi(getNextDollarDelimtedValue(f, 10));

    char *board = (char *)malloc(size * sizeof(char));

    board = getNextDollarDelimtedValue(f, 1000);

    fclose(f);

    struct board b = {size, width, strToUByteArray(board, size)};
    printf("size: %d \n", size);

    return b;
}

