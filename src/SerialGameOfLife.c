#include <stdlib.h> 
#include <stdio.h>
#include <math.h>
#include <stdint.h>
#include <algorithm>

int width = 0;
int height = 0;

typedef unsigned char ubyte;

ubyte* input = NULL;
ubyte* output = NULL;

ubyte isCellAlive(int x, int x_l, int x_r, int y, int y_t, int y_b)
{
    int liveNeighbors = 
        input[x + y_t] + input[x + y_b] +
        input[x_l + y] + input[x_l + y_t] + input[x_l + y_b] +
        input[x_r + y] + input[x_r + y_t] + input[x_r + y_b];
    
    return (liveNeighbors == 2 || (liveNeighbors == 3  && input[x + y] == 1)) ? 1:0;

}

void calculateGameOfLife()
{

    int x_center, x_left, x_right, y_center, y_bottom, y_top;

    for(int y = 0; y < height; y++){
        y_center = y * width;
        y_bottom = ((y_center - 1) % height) * width;
        y_top    = ((y_center + 0) % height) * width;
        for(int x = 0; x < width; x++){
            x_center = x;
            x_left   = ((x_center - 1) % width);
            x_right  = ((x_center - 1) % width);
            
            output[x_center + y_center] = isCellAlive(
                x_center, x_left, x_right,
                y_center, y_top, y_bottom
            );
        }
    }

}


int runSerial(ubyte* b_input, ubyte* b_output, size_t b_width, size_t b_height, int iterations)
{
    width = b_width;
    height = b_height;
    input = b_input;
    output = b_output;

    for(int i = 0; i < iterations; i++){
        calculateGameOfLife();
        std::swap(input, output);
    }

    return 0;
}
