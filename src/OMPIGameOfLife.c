#include <cstdio>
#include <math.h>
#include <stdlib.h>
#include <iostream>
#include <cstring>
#include <omp.h>
#include <mpi.h>
#include <time.h>
#include <string>
#include <stdexcept>
#include "BoardReader.c"
#include "common.c"

using namespace std;

typedef unsigned char ubyte;

inline ubyte isCellAlive(ubyte* arr, ubyte* top_g, ubyte* bottom_g, int height, int x, int x_l, int x_r, int y, int y_t, int y_b)
{

    /*  Calculate live neighbors. 
     *  Being sure to use ghost cells for the top and bottom rows!
     */

    int liveNeighbors;
    if (y == height - 1)
    {
      liveNeighbors = 
        top_g[x + y_t] + arr[x + y_b] +
        arr[x_l + y] + top_g[x_l + y_t] + arr[x_l + y_b] +
        arr[x_r + y] + top_g[x_r + y_t] + arr[x_r + y_b];
    }else if (y == 0)
    {
      liveNeighbors = 
        arr[x + y_t] + bottom_g[x + y_b] +
        arr[x_l + y] + arr[x_l + y_t] + bottom_g[x_l + y_b] +
        arr[x_r + y] + arr[x_r + y_t] + bottom_g[x_r + y_b];
    }else 
    {
      liveNeighbors = 
        arr[x + y_t] + arr[x + y_b] +
        arr[x_l + y] + arr[x_l + y_t] + arr[x_l + y_b] +
        arr[x_r + y] + arr[x_r + y_t] + arr[x_r + y_b]; 
    }
    return (liveNeighbors == 2 || liveNeighbors == 3) ? 1:0;

}

/* Define macros for the neighbors to avoid
 * using extra storage and data movement
 * while keeping the code readable
 */

#define YCENTER (y * local_width)
#define YBOTTOM (((YCENTER - 1) % local_height) * local_width)
#define YTOP (((YCENTER + 0) % local_height) * local_width)
#define XCENTER x 
#define XLEFT (((XCENTER - 1) % local_width))
#define XRIGHT (((XCENTER - 1) % local_width))


/* given an array, calculate a checksum in parallel
 */

inline ubyte get_check_sum(ubyte* arr, int size)
{
    ubyte sum = 0;
    #pragma omp parallel for reduction (+: sum)
    for(int i = 0; i < size; i++)
    {
        sum += arr[i];
    }
}


// populate buffer to send,  with data from local buffer
inline void update_to_send(ubyte* send_buff_top, ubyte* send_buff_bottom, ubyte* data_buff, int height, int width)
{
    for(int i = 0; i < height; i++)
    {
        if(i == 0)
        {
            for(int j = 0; j < width; j++)
            {
                send_buff_bottom[j] = data_buff[j];
            }
        }
        if(i == height - 1)
        {
            for(int j = 0; j < width; j++)
            {
                send_buff_top[j] = data_buff[i * width + j];
            }

        }     
    }
}



int main(int argc, char** argv)
{
    MPI_Init(&argc, &argv);

    int world_size;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);

    int world_rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

    if(world_rank == 0 && argc < 4){
        printf("Not enough arguments. usage: \n ./game_mpi <iterations> <board-size> <board-width> \n");
        return 1;
    }

    int iterations = atoi(argv[1]);
    int bsize      = atoi(argv[2]);
    int bwidth     = atoi(argv[3]);
    int bheight    = bsize / bwidth;

    // declare global variables 
    ubyte* global_cells;    
    ubyte* global_output;
    ubyte* global_ghosts;
    board global_board;
    board local_board;
    ubyte g_check_sum;  // when done, use gather to get all the partial checksums

    uint64_t global_t = 0;
    uint64_t local_t = 0;
    uint64_t start_t;
    uint64_t end_t;
    InitTSC();


    // declare local variables
    ubyte l_check_sum = 0;
    ubyte* local_cells; 
    ubyte* local_output;
    int local_width  = bwidth;
    int local_height = bheight / world_size;
    int local_size   = local_width * local_height;


    // initialize global buffer on root
    if(world_rank == 0)
    {   
        global_cells  = new ubyte[bsize];
        global_board  = {bsize, bwidth, global_cells};
        gen_random_board(&global_board);
    }

    // allocate local buffers for ghost cells
    ubyte* local_ghosts    = new ubyte[local_width * 2];
    ubyte* to_send         = new ubyte[local_width * 2];

    int lg_start_top    = 0;
    int lg_end_top      = local_width - 1;
    int lg_start_bottom = lg_end_top + 1;
    int lg_end_bottom   = lg_start_bottom + local_width - 1;

    // declare pointers for top vs bottom ghost cells
    ubyte* lg_top    = &local_ghosts[lg_start_top];
    ubyte* lg_bottom = &local_ghosts[lg_start_bottom];

    ubyte* to_send_top    = &to_send[0];
    ubyte* to_send_bottom = &to_send[local_width];

    // allocate & initialize space for local (non-ghost) buffers
    local_cells  = new ubyte[local_size];
    local_output = new ubyte[local_size];
    memset(local_output, 0, local_size);
    

    

    // send data to all processes
    MPI_Scatter(global_cells, local_size, MPI_UNSIGNED_CHAR, local_cells, local_size, MPI_UNSIGNED_CHAR, 0, MPI_COMM_WORLD);

    // initialize ghost cells before starting iteration loop
    update_to_send(to_send_top, to_send_bottom, local_cells, local_height, local_width);

    // start actual iterations
    start_t = ReadTSC();
    for(int it = 0; it < iterations; it++)
    {
        // send your top ghost row to your upper neighbor unless you're topmost
        if(world_rank != world_size - 1)
        {
            MPI_Send(to_send_top, local_width, MPI_UNSIGNED_CHAR, world_rank + 1, 1, MPI_COMM_WORLD);
        }

        // receive bottom row from your lower neighbor unless you're bottom-most
        // also send your bottom row to them
        if(world_rank != 0)
        {
            MPI_Recv(lg_bottom, local_width, MPI_UNSIGNED_CHAR, world_rank - 1, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            MPI_Send(to_send_bottom, local_width, MPI_UNSIGNED_CHAR, world_rank - 1, 0, MPI_COMM_WORLD);
        }

        // receive top row from your upper neighbor unless you're topmost
        if(world_rank != world_size - 1)
        {
            MPI_Recv(lg_top, local_width, MPI_UNSIGNED_CHAR, world_rank + 1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        }
        
        // do the computation in parallel making use of ghost cells
        #pragma omp parallel for 
        for(int y = 0; y < local_height; y++){ 
            #pragma omp parallel for 
            for(int x = 0; x < local_width; x++){
                local_output[XCENTER + YCENTER] = isCellAlive(local_cells, 
                                                          lg_top, 
                                                          lg_bottom,
                                                          local_height,
                                                          XCENTER, XLEFT, XRIGHT,
                                                          YCENTER, YTOP, YBOTTOM);  
            }
        }  
        MPI_Barrier(MPI_COMM_WORLD); // synchronize threads before swapping 
        swap(local_cells, local_output);
        // update ghost cells to send in the next iteration unless you're done iterating.
        if(it != iterations - 1)
        {
            update_to_send(to_send_top, to_send_bottom, local_cells, local_height, local_width);
        }
    }
    end_t   = ReadTSC();
    local_t += ElapsedTime(end_t - start_t);
    MPI_Reduce(&local_t, &global_t, 1, MPI_UINT64_T, MPI_SUM, 0, MPI_COMM_WORLD);

    // calculate local check_sum once done
    l_check_sum += get_check_sum(local_cells, local_height * local_width);

    // reduce all checksums into the global one
    MPI_Reduce(&l_check_sum, &g_check_sum, 1, MPI_UNSIGNED_CHAR, MPI_SUM, 0, MPI_COMM_WORLD);


    // clean up
    delete[] local_cells;
    delete[] to_send;
    delete[] local_ghosts;
    delete[] local_output;

    if(world_rank == 0)
    {
        delete[] global_cells; // clean up
        printf("global checksum: %d \n", g_check_sum); // print checksum
        printf("Time to complete parallel run with mpi: %lu (s) \n", global_t);
        printf("DONE \n");
    }

    MPI_Finalize();
    return 0;
}
