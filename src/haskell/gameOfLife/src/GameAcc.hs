{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE RebindableSyntax    #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeOperators       #-}
{-# LANGUAGE ViewPatterns        #-}




module GameAcc  where

import Data.Array.Accelerate                              as A
import qualified Data.Vector.Unboxed as U
-- import Control.Monad.Random 

import qualified Prelude    as P                                          -- ( fromInteger )

type Health = Int 

type Board  = Array DIM2 Int  


update_life_acc :: A.Exp Int -> Acc (Array DIM1 Int) -> Exp Int 
update_life_acc b neighbors = let n = the $ sum neighbors 
                              in if (b /= 0 :: Exp Bool) then 
                                            if  (n > 3)  
                                                then 0 
                                            else 
                                                if (n < 2) then 0 else  1
                                 else if (n == 3) then 1 else 0 




update_board_acc :: Acc Board -> Exp Int -> Acc (Board)
update_board_acc b w = imap new_func b 
           where
            neighbors ::Array DIM1 (Int, Int) 
            neighbors = A.fromList (Z :. 9) [(-1, -1), (0, -1), (1, -1), (-1, 0), (0, 0), (1, 0), (-1, 1), (0, 1), (1, 1)] 

            get_neighbors :: (Exp DIM2) -> Exp Int -> Acc (Array DIM1 (Bool, DIM2))
            get_neighbors pat w = let (unlift -> (Z :. x :. y)) = pat in map (\p -> case p of 
                                                                          (unlift -> (e1, e2)) ->   let 
                                                                                                       (t1 :: Exp (Plain (Exp Bool, Exp DIM2))) = lift (constant True, index2 (e1 + x) (e2 + y))
                                                                                                       (t2 :: Exp (Plain (Exp Bool, Exp DIM2))) =  lift (constant False, index2 (e1 + x) (e2 + y))
                                                                                                    in (if (e1 + x < 0 || e1 + x >= w) 
                                                                                                        then if (e2 + y < 0 || (e2 + y) > w) 
                                                                                                             then t1 
                                                                                                             else t2 
                                                                                                        else t2)) (use neighbors)

            new_func :: Exp (Z :. Int :. Int) -> Exp Int ->  Exp Int  
            new_func pat elem = update_life_acc elem $ map (\j -> case j of   
                                                                (unlift -> (f, sh)) -> if f == (constant True) then b ! sh :: Exp Int
                                                                                       else constant 0) (get_neighbors pat w) 



                                                   

                              
                                

               









