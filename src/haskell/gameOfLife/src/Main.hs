module Main where 

import Data.List (unfoldr)
import Game 
import Graphics.Gloss 
import Graphics.Gloss.Data.ViewPort
import Control.Monad.Random 
import System.Environment 



h_to_color :: Health -> Color 
h_to_color 0 = white 
h_to_color 1 = black 
h_to_color _ = error "color is either black or white!" 


c_to_health :: Color -> Health 
c_to_health white = 0 
c_to_health black = 1 
c_to_health _     = error "unrecognized color!"


type CBoard = ((Int, Int) -> Color, Int) 

color_board :: Board -> CBoard 
color_board (f, w) = (h_to_color . f, w)

uncolor_board :: CBoard -> Board 
uncolor_board (func, w) = (c_to_health . func, w)


update_cboard :: CBoard -> CBoard 
update_cboard = color_board . update_board . uncolor_board 


render_next :: ViewPort -> Float -> CBoard -> CBoard 
render_next _ _ = update_cboard 



computeCBoard :: CBoard -> Picture 
computeCBoard (f, w) = pictures $ unfoldr (picture_c f) ((w * w))  
                where 
                    depict_c white = blank 
                    depict_c black = color black blank 

                    picture_c func n | n < 0     = Nothing 
                                     | otherwise = let loc = n `divMod` w 
                                                   in 
                                                    Just (depict_c $ func loc, n - 1) 



initCBoard :: (RandomGen g) => Int -> Rand g CBoard
initCBoard w = color_board <$> rand_board w  



main :: IO ()
main = do 
    args <- getArgs
    let 
        my_args = take 2 args 
        w       = read $ head my_args :: Int 
        iter    = read $ last my_args :: Int  -- simulations per second!
    init_board <- evalRandIO $ initCBoard w 
    simulate (InWindow "GoL -- Serial" (600, 800) (10, 10)) -- or FullScreen 
             black
             iter
             init_board
             computeCBoard 
             render_next  
             
    return ()







                                    







