
module Game  where


import qualified Data.Vector.Unboxed as U
import Control.Monad.Random 

import Prelude                                           -- ( fromInteger )

type Health = Int 

type Board  = ((Int, Int) -> Health, Int)  -- indexing function, width of board

update_life :: Int -> [Int] -> Health  
update_life b neighbors  = let n = sum neighbors 
                           in if b /= 0 then  if n > 3 then 0 
                                 else 
                                    if n < 2 then 0 
                                    else 1
                              else if n == 3 then 1 else 0 


update_board :: Board -> Board 
update_board (func, w) = (new_func w, w)
           where
            neighbors :: [(Int, Int)]  
            neighbors = [(-1, -1), (0, -1), (1, -1), (-1, 0), (0, 0), (1, 0), (-1, 1), (0, 1), (1, 1)] 

            get_neighbors :: (Int, Int) -> Int -> [Maybe (Int, Int)]
            get_neighbors (x, y) w = map (\(e1, e2) -> case (or [e1 + x < 0, e1 + x >= w], or [e2 + y < 0, e2 + y >= w]) of 
                                                         (False, False) -> Just (e1 + x, e2 + y)
                                                         _              -> Nothing) neighbors

            new_func :: Int -> (Int, Int)  -> Health 
            new_func w loc = update_life (func loc) $ map (\j -> case j of 
                                                                Nothing -> 0
                                                                Just e  -> func e) (get_neighbors loc w) 



computeBoard :: Board -> U.Vector Health
computeBoard (f, w) = U.generate (w * w) (\i -> let (r, c) = i `divMod` w in f (r, c))


fromVector :: U.Vector Health -> Board 
fromVector vec = (func, w) 
           where  
              len         = U.length vec 
              w           = floor $ sqrt $ fromInteger $ toInteger len    
              f           = (U.!) vec 
              func (x, y) = f (x * w + y) 


-------------------- initialization --------------------------------
rand_health :: (RandomGen g) => Rand g Health 
rand_health = getRandomR (0,1)


rand_board_vec :: (RandomGen g) => Int -> Rand g (U.Vector Health) 
rand_board_vec w = U.replicateM (w * w) rand_health


rand_board :: (RandomGen g) => Int -> Rand g Board 
rand_board w = fromVector <$> rand_board_vec w  
                                                   

                              
                                

               









