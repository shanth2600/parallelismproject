#include "BoardReader.c"
#include "GPUGameOfLifeMC.cu"
#include "SerialGameOfLife.c"
#include "GPUBitwiseGameOfLife.cu"


int main(int argc, char **argv)
{

    const int CORES = 1024;

    int width = 0;
    int height = 0;
    int size = 0;

    uint64_t start_t;
    uint64_t end_t;

    InitTSC();

    if (argc < 4)
    {
        printf("Not enough arguments. usage: \n ./GPUBitwiseGameOfLife <iterations> <board-size> <board-width> \n");
        return 1;
    }

    int iterations = atoi(argv[1]);

    size = atoi(argv[2]);
    width = atoi(argv[3]);
    height = size / width;

    ubyte *s_input = new ubyte[size];
    ubyte *s_output = new ubyte[size];
    ubyte *g_input = new ubyte[size];
    ubyte *g1_input = new ubyte[size];
    ubyte *g2_input = new ubyte[size];

    board b = {size, width, g_input};
    gen_random_board(&b);
    memcpy(g1_input, g_input, size);
    memcpy(g2_input, g_input, size);
    memcpy(s_input, g_input, size);

    ubyte *g_output = new ubyte[size];
    ubyte *g1_output = new ubyte[size];
    ubyte *g2_output = new ubyte[size];
    ubyte *d_input = NULL;
    ubyte *d_output = NULL;
    ubyte *d1_input = NULL;
    ubyte *d1_output = NULL;
    ubyte *d2_input = NULL;
    ubyte *d2_output = NULL;

    // Serial

    start_t = ReadTSC();
    runSerial(s_input, s_output, width, height, iterations);
    end_t = ReadTSC();

    printf("Time to complete serial run: %g (s) \n", ElapsedTime(end_t - start_t));
    
    // run memory coalesced version

    start_t = ReadTSC();
    cudaMalloc((void **)&d1_input, size);
    cudaMalloc((void **)&d1_output, size);
    cudaMemcpy(d1_input, g1_input, size, cudaMemcpyHostToDevice);
    runMemKernel(d1_input, d1_output, width, height, iterations, CORES);
    cudaMemcpy(g1_output, d1_output, size, cudaMemcpyDeviceToHost);
    end_t = ReadTSC();
    printf("Time to complete GPU Memory coalesced run: %g (s) \n", ElapsedTime(end_t - start_t));

    cudaFree(&d1_input);
    cudaFree(&d1_output);

    // run bitwise optimized one

    start_t = ReadTSC();
    cudaMalloc((void **)&d2_input, size);
    cudaMalloc((void **)&d2_output, size);
    cudaMemcpy(d2_input, g2_input, size, cudaMemcpyHostToDevice);
    runKernel(d2_input, d2_output, width, height, iterations, CORES);
    cudaMemcpy(g2_output, d2_output, size, cudaMemcpyDeviceToHost);
    end_t = ReadTSC();
    printf("Time to complete GPU Bitwise run: %g (s) \n", ElapsedTime(end_t - start_t));

    cudaFree(&d2_input);
    cudaFree(&d2_output);

    return 0;
}