#include <algorithm>

typedef unsigned char ubyte;

__global__ void gameOfLifeKernel(const ubyte* input, uint width, uint height, ubyte* output)
{
    uint size = width * height;
 
    for (uint eId = __mul24(blockIdx.x, blockDim.x) + threadIdx.x; eId < size; eId += blockDim.x * gridDim.x) {
        int x_center = eId % width;
        int x_left = (x_center + width - 1) % width;
        int x_right = (x_center + 1) % width;
        int y_center = eId - x_center;
        int y_top = (y_center + size - width) % size;
        int y_bottom = (y_center + width) % size;
        
        int liveNeighbors = 
            input[x_center + y_top] + input[x_center + y_bottom] +
            input[x_left + y_center] + input[x_left + y_top] + input[x_left + y_bottom] +
            input[x_right + y_center] + input[x_right + y_top] + input[x_right + y_bottom];
 
        output[x_center + y_center] =
            liveNeighbors == 3 || (liveNeighbors == 2 && input[x_center + y_center] == 1) ? 1 : 0;
    }
}

void runMemKernel(ubyte*& input, ubyte*& output, size_t width, size_t height, size_t iterationsCount, ushort threadsCount) {
    size_t blocksCount = (width * height) / threadsCount;
    for (size_t i = 0; i < iterationsCount; ++i) {
        gameOfLifeKernel<<<blocksCount, 256>>>(input, width, height, output);
        std::swap(input, output);
    }
}
