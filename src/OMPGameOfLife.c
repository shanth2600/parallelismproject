#include <cstdio>
#include <math.h>
#include <stdlib.h>
#include <iostream>
#include <cstring>
#include <omp.h>
#include <time.h>
#include <string>
#include <stdexcept>
#include "BoardReader.c"
#include "common.c"

using namespace std;

typedef unsigned char ubyte;

inline ubyte isCellAlive(ubyte* arr, int x, int x_l, int x_r, int y, int y_t, int y_b)
{
    int liveNeighbors = 
        arr[x + y_t] + arr[x + y_b] +
        arr[x_l + y] + arr[x_l + y_t] + arr[x_l + y_b] +
        arr[x_r + y] + arr[x_r + y_t] + arr[x_r + y_b];
    
    return (liveNeighbors == 2 || liveNeighbors == 3) ? 1:0;

}

#define YCENTER (y * width)
#define YBOTTOM (((YCENTER - 1) % height) * width)
#define YTOP (((YCENTER + 0) % height) * width)
#define XCENTER x 
#define XLEFT (((XCENTER - 1) % width))
#define XRIGHT (((XCENTER - 1) % width))

inline void calculateGameOfLife(ubyte* arr1, ubyte* output, int height, int width)
{
    for(int y = 0; y < height; y++){
        for(int x = 0; x < width; x++){
            output[XCENTER + YCENTER] = isCellAlive(arr1, 
                                                      XCENTER, XLEFT, XRIGHT,
                                                      YCENTER, YTOP, YBOTTOM);  
        }
    }

}

ubyte get_check_sum(ubyte* arr, int size)
{
    ubyte sum = 0;
    #pragma omp parallel for reduction (+: sum)
    for(int i = 0; i < size; i++)
    {
        sum += arr[i];
    }
}


inline ubyte calculateGameOfLifeParallel(ubyte* inp, ubyte* out, int arr_size, ubyte check_omp, int iterations, int height, int width)
{
    for(int i = 0; i < iterations; i++)
    {
        calculateGameOfLife(inp, out, height, width);
        // // swap(inp, out);
        check_omp += get_check_sum(out, arr_size);
        ubyte* temp = inp;
        inp = out;
        ubyte* out = temp;
    }
    return check_omp;

}


int main(int argc, char** argv)
{

    if(argc < 4){
        printf("Not enough arguments. usage: \n ./game_omp <iterations> <board-size> <board-width> \n");
        return 1;
    }
    uint64_t start_t;
    uint64_t end_t;
    InitTSC();

    int iterations = atoi(argv[1]);

    // struct board b = readBoard("board.txt");
    int bsize  = atoi(argv[2]);
    int bwidth = atoi(argv[3]);
    ubyte* cells = new ubyte[bsize];//malloc(bsize * sizeof(ubyte));
    board b = {bsize, bwidth, cells};
    gen_random_board(&b);

    int width = b.width;
    int height = b.size / b.width;

    //for serial
    ubyte* input = b.cells; 
    int arr_size = bsize * sizeof(ubyte);
    ubyte* output = new ubyte[bsize]; 
    memset(output, 0, arr_size);

    ubyte check_serial = get_check_sum(input, arr_size);

    //for openMP 
    ubyte* output_omp = (ubyte *) malloc(arr_size); 
    ubyte* input_omp  = (ubyte *) malloc(arr_size); 
    memcpy(input_omp, input, arr_size);
    memset(output_omp, 0, arr_size);

    printf("checksum before serial run: %d \n", check_serial);

   
    start_t = ReadTSC();
    for(int i = 0; i < iterations; i++){
        for(int y = 0; y < height; y++){
            for(int x = 0; x < width; x++){
                output[XCENTER + YCENTER] = isCellAlive(input, 
                                                          XCENTER, XLEFT, XRIGHT,
                                                          YCENTER, YTOP, YBOTTOM);  
            }
        }
        swap(input, output);
    }
    end_t = ReadTSC();

    check_serial += get_check_sum(output, arr_size);
    printf("checksum after serial run (iter = %d): %d \n", iterations, check_serial);
    printf("Time to complete serial run: %g (s) \n", ElapsedTime(end_t - start_t));


    // open MP implementation for one node
    ubyte check_omp = get_check_sum(input_omp, arr_size);
    printf("checksum before parallel run (iter = %d): %d \n", iterations, check_omp);
    start_t = ReadTSC();
    for(int i = 0; i < iterations; i++){
        #pragma omp parallel for 
        for(int y = 0; y < height; y++){
            #pragma omp parallel for 
            for(int x = 0; x < width; x++){
                output_omp[XCENTER + YCENTER] = isCellAlive(input_omp, 
                                                          XCENTER, XLEFT, XRIGHT,
                                                          YCENTER, YTOP, YBOTTOM);  
            }
        }
        swap(input_omp, output_omp);
    }
    end_t = ReadTSC();

   
    check_omp += get_check_sum(output_omp, arr_size);
    printf("checksum after openMP run: %d \n", check_omp);
    printf("Time to complete parallel run with omp: %g (s) \n", ElapsedTime(end_t - start_t));
    
    if(check_serial == check_omp)
    {
        printf("implementation success! \n");
    }
    delete[] output;
    delete[] input_omp;
    delete[] output_omp;

    return 0;
}
